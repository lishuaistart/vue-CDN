const path = require('path')
const webpack = require('webpack')

function resolve(dir) {
  return path.join(__dirname, dir)
}
// 是否为生产环境
const isProduction = process.env.NODE_ENV !== 'development'
// gzip压缩
const CompressionWebpackPlugin = require('compression-webpack-plugin')

const assetsCDN = {
  // webpack build externals
  externals: {
    vue: 'Vue',
    VueRouter: 'VueRouter',
    vuex: 'Vuex',
    axios: 'axios',
    moment: 'moment',
    VueI18n: 'VueI18n',
    // 'antd': 'antd',
    ECharts: 'echarts',
    SparkMD5: 'SparkMD5',
    QRCode: 'QRCode',
    VueAMap: 'VueAMap',
    infiniteScroll: 'infiniteScroll',
    vuedraggable: 'draggable',
    WebsocketHeartbeatJs: 'WebsocketHeartbeatJs',
    AMap: 'AMap',
    Element: 'element-ui',
  },
  css: [
    'https://cdn.jsdelivr.net/npm/vue-croppa@1.3.8/dist/vue-croppa.min.css',
  ],
  js: [
    'https://cdn.jsdelivr.net/npm/vue@2/dist/vue.min.js',
    'https://cdn.jsdelivr.net/npm/vue-router@3.1.3/dist/vue-router.min.js',
    'https://cdn.jsdelivr.net/npm/vuex@3.1.1/dist/vuex.min.js',
    'https://cdn.jsdelivr.net/npm/axios@0.19.2/dist/axios.min.js',
    'https://cdn.jsdelivr.net/npm/moment@2/moment.min.js',
    'https://cdn.jsdelivr.net/npm/moment@2/locale/zh-cn.min.js',
    'https://cdn.jsdelivr.net/npm/lodash@4/lodash.min.js',
    'https://cdn.bootcdn.net/ajax/libs/vue-i18n/8.8.0/vue-i18n.min.js',
    'https://cdn.jsdelivr.net/npm/ant-design-vue@1/dist/antd.min.js',
    'https://cdn.jsdelivr.net/npm/ant-design-vue@1/dist/antd-with-locales.min.js',
    'https://cdn.jsdelivr.net/npm/echarts@4/dist/echarts.min.js',
    'https://cdn.jsdelivr.net/npm/spark-md5@3.0.1/spark-md5.min.js',
    'https://webapi.amap.com/maps?key=02b2d70ea2143d0000d17e5a26a16b1b&v=1.4.15&plugin=AMap.MarkerClusterer',
    'https://webapi.amap.com/loca?key=02b2d70ea2143d0000d17e5a26a16b1b&v=1.3.0',
    'https://cdn.jsdelivr.net/npm/vue-amap@0.5.10/dist/index.min.js',
    'https://cdn.jsdelivr.net/npm/vue-infinite-scroll@2.0.2/vue-infinite-scroll.min.js',
    'https://cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js',
    'https://cdn.jsdelivr.net/npm/vuedraggable@2.23.0/dist/vuedraggable.umd.min.js',
    'https://cdn.jsdelivr.net/npm/websocket-heartbeat-js@1.0.11/dist/index.min.js',
    'http://pv.sohu.com/cityjson?ie=utf-8',
    'https://unpkg.com/element-ui/lib/index.js',
  ],
}
// vue.config.js
const vueConfig = {
  publicPath: './',
  assetsDir: '',
  filenameHashing: false,
  configureWebpack: {
    // webpack plugins
    plugins: [
      // Ignore all locale files of moment.js
      // new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    ],
    externals: assetsCDN.externals,
    resolve: {
      alias: {
        assets: resolve('src/assets'),
        views: resolve('src/views'),
        '@': resolve('src/components'),
        '#': resolve('src'),
        utils: resolve('utils'),
      },
    },
    // 生产环境相关配置
  },

  chainWebpack: (config) => {
    config.plugins.delete('preload')
    // 移除 prefetch 插件
    config.plugins.delete('prefetch')
    config
      .plugin('compression')
      .use(CompressionWebpackPlugin)
      .tap(() => [
        {
          test: /\.ttf|\.js$|\.html$|\.css/, // 匹配文件名
          threshold: 1024, // 超过1k进行压缩
          minRatio: 0.8, // 只有压缩率小于这个值的资源才会被处理
          deleteOriginalAssets: false, // 是否删除源文件
        },
      ])
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .oneOf('inline')
      .resourceQuery(/inline/)
      .use('vue-svg-icon-loader')
      .loader('vue-svg-icon-loader')
      .end()
      .end()
      .oneOf('external')
      .use('file-loader')
      .loader('file-loader')
      .options({
        name: 'assets/[name].[hash:8].[ext]',
      })
    const oneOfsMap = config.module.rule('scss').oneOfs.store
    oneOfsMap.forEach((item) => {
      item
        .use('sass-resources-loader')
        .loader('sass-resources-loader')
        .options({
          // Provide path to the file with resources
          // 要公用的scss的路径
          resources: ['src/assets/css/mixin.scss', 'src/assets/css/base.scss'],
        })
        .end()
    })
    // assets require on cdn
    config.plugin('html').tap((args) => {
      args[0].cdn = assetsCDN
      return args
    })
  },
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          // less vars，customize ant design theme

          // 'primary-color': '#F5222D',
          // 'link-color': '#F5222D',
          'border-radius-base': '2px', // 组件/浮层圆角
          'success-color': '#2faf80', // 成功色
          'warning-color': '#f26f3e', // 警告色
          'error-color': '#ee392b', // 错误色
          'disabled-color': '#cccccc', // 失效色
          'border-color-base': '#dedede', // 边框色
          'border-color-split': '#dedede',
          'text-color': '#303030', // 主文本色
          'heading-color': '#303030',
          'text-color-secondary': '#606060', // 次文本色
          'descriptions-bg': '#f8f8f8',
        },
        // DO NOT REMOVE THIS LINE
        javascriptEnabled: true,
      },
    },
  },

  devServer: {
    // development server port 8000
    port: 8000,
    // If you want to turn on the proxy, please remove the mockjs /src/main.jsL11
    // proxy: {
    //   '/api': {
    //     target: 'https://mock.ihx.me/mock/5baf3052f7da7e07e04a5116/antd-pro',
    //     ws: false,
    //     changeOrigin: true
    //   }
    // }
  },

  // disable source map in production
  productionSourceMap: false,

  lintOnSave: undefined,

  // babel-loader no-ignore node_modules/*
  transpileDependencies: [],

  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'less',
      patterns: [],
    },
  },
}
console.log(process.env.NODE_ENV)
// preview.pro.loacg.com only do not use in your production;
if (process.env.VUE_APP_PREVIEW === 'true') {
  console.log('VUE_APP_PREVIEW', true)
}
module.exports = vueConfig
