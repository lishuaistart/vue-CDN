/*
 * @Description:
 * @Version: 1.0.0
 * @Author: 李晨光
 * @Date: 2020-08-20 14:11:46
 * @LastEditors: 李帅
 * @LastEditTime: 2021-07-17 13:41:47
 */

import { SET_NAME } from './mutationTypes'
export default {
  [SET_NAME](state, name) {
    state.name = name
  },
}
