/*
 * @Descripttion: getter
 * @Version: 1.0.0
 * @Author: 李晨光
 * @Date: 2020-07-07 13:46:46
 * @LastEditors: 李帅
 * @LastEditTime: 2021-07-17 13:42:35
 */

export const name = (state) => {
  return state.name
}
