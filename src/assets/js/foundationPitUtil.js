/*
 * @Description: 
 * @Version: 1.0.0
 * @Author: 赵亮
 * @Date: 2021-03-16 14:50:37
 * @LastEditors: 赵亮
 * @LastEditTime: 2021-06-18 15:16:19
 * 深基坑
 */
import * as echarts from 'echarts'
let format = (value) => {
  return value >= 10 ? value : '0' + value
}

function formatNumber(n) {
  return n.toString().length > 1 ? n : '0' + n;
}
// 判断空
export const isNull = (data) => {
  if (typeof data == "undefined" || data == null || data == '' || (typeof data === "object" && (data instanceof Array) && data.length == 0)) {
    return true;
  } else {
    if (typeof data === "object" && !(data instanceof Array)) {
      var hasProp = false;
      for (var prop in data) {
        hasProp = true;
        break;
      }
      if (!hasProp) {
        return true;
      }
    }
    return false;
  }
}
// 获取点位图标
export const getPosIcon = function (data) {
  if (data.sensorType == 'WaterData' || data.sensorType == 'Yuliang' || data.sensorType == 'Trwsd') {
    // 水位传感器
    if (data.status == 1) {
      // 正常
      return require('../image/SJKImg/shuiwei/shuiwei01.png')
    } else if (data.status == 2) {
      // 告警
      return require('../image/SJKImg/shuiwei/shuiwei03.png')
    } else if (data.status == 3) {
      // 预警
      return require('../image/SJKImg/shuiwei/shuiwei02.png')
    } else if (data.status == 4) {
      // 离线
      return require('../image/SJKImg/shuiwei/shuiwei04.png')
    }
  } else if (data.sensorType == 'ZhenXianTempData' || data.sensorType == 'CollZhouliPoint' || data.sensorType == 'CODE_005' || data.sensorType == 'YingLi') {
    // 锚杆拉力 轴力
    if (data.status == 1) {
      // 正常
      return require('../image/SJKImg/yingli/yingli01.png')
    } else if (data.status == 2) {
      // 告警
      return require('../image/SJKImg/yingli/yingli03.png')
    } else if (data.status == 3) {
      // 预警
      return require('../image/SJKImg/yingli/yingli02.png')
    } else if (data.status == 4) {
      // 离线
      return require('../image/SJKImg/yingli/yingli04.png')
    }
  } else if (data.sensorType == 'CollWeiyiPoint') {
    // 位移
    if (data.status == 1) {
      // 正常
      return require('../image/SJKImg/weiyi/weiyi01.png')
    } else if (data.status == 2) {
      // 告警
      return require('../image/SJKImg/weiyi/weiyi03.png')
    } else if (data.status == 3) {
      // 预警
      return require('../image/SJKImg/weiyi/weiyi02.png')
    } else if (data.status == 4) {
      // 离线
      return require('../image/SJKImg/weiyi/weiyi04.png')
    }
  } else if (data.sensorType == 'CollQingxiePoint') {
    // 倾角
    if (data.status == 1) {
      // 正常
      return require('../image/SJKImg/qingjiao/qingjiao01.png')
    } else if (data.status == 2) {
      // 告警
      return require('../image/SJKImg/qingjiao/qingjiao03.png')
    } else if (data.status == 3) {
      // 预警
      return require('../image/SJKImg/qingjiao/qingjiao02.png')
    } else if (data.status == 4) {
      // 离线
      return require('../image/SJKImg/qingjiao/qingjiao04.png')
    }
  }
}
// 获取地图范围
export const mapRange = function (fileSize) {
  let aspectRatio = (
    fileSize.split('*')[0] /
    fileSize.split('*')[1]
  ).toFixed(1) // 宽高比
  let range = {
    leftTop: [],
    rightBottom: [],
    zoom: ''
  }
  if (aspectRatio > 0.8 && aspectRatio < 1.2) {
    // 正方形
    range.leftTop = [
      Number(66.53427) - Number(0.03),
      Number(67.4712) - Number(0.01)
    ]
    range.rightBottom = [
      Number(66.53427) + Number(0.03),
      Number(67.4712) + Number(0.01)
    ]
    range.zoom = 14
  } else if (aspectRatio < 0.8 || aspectRatio == 0.8) {
    // 竖长方形
    range.leftTop = [
      Number(66.53427) - Number(0.03),
      Number(67.4712) - Number(0.018)
    ]
    range.rightBottom = [
      Number(66.53427) + Number(0.03),
      Number(67.4712) + Number(0.018)
    ]
    range.zoom = 13
  } else if (aspectRatio > 1.2 || aspectRatio == 1.2) {
    // 横长方形
    range.leftTop = [
      Number(66.53427) - Number(0.038),
      Number(67.4712) - Number(0.01)
    ]
    range.rightBottom = [
      Number(66.53427) + Number(0.038),
      Number(67.4712) + Number(0.01)
    ]
    range.zoom = 14
  }
  return range
}
// 时间转换
export const dateFilter = (time, type) => {
  let date = new Date(time * 1000)
  let year = date.getFullYear()
  let month = date.getMonth() + 1
  let day = date.getDate()
  let hours = date.getHours()
  let minutes = date.getMinutes()
  let second = date.getSeconds()
  let result
  switch (type) {
    case 0: // 01-05
      result = `${format(month)}月${format(day)}日`
      break
    case 1: // 11:12
      result = `${format(hours)}-${format(minutes)}`
      break
    case 2: // 2015-01-05
      result = `${year}-${format(month)}-${format(day)}`
      break
    case 3: // 2015-01-05 11:12
      result = `${year}-${format(month)}-${format(day)}  ${format(hours)}:${format(minutes)}`
      break
    case 4: // 2015.01.05 11:12
      result = `${year}.${month}.${day}  ${hours}:${minutes}`
      break
    case 5:
      result = `${year}年${format(month)}月${format(day)}日${format(hours)}时${format(minutes)}分`
    case 6: // 2015-01-05 11:12:06
      result = `${year}-${format(month)}-${format(day)}  ${format(hours)}:${format(minutes)}:${format(second)}`
      break
    case 7:
      result = `${year}-${format(month)}`
      break
    case 8:
      result = `${year}年${format(month)}月`
      break
  }
  return result
};
// 获取前一天
export const preDay = () => {
  var nowdate = new Date();
  var preDate = new Date(nowdate.getTime() - 24 * 60 * 60 * 1000); //前一天
  return dateFilter(preDate / 1000, 2)
}
// 获取上周
export const preWeek = () => {
  var nowdate = new Date();
  var oneweekdate = new Date(nowdate - 7 * 24 * 3600 * 1000);
  var y = oneweekdate.getFullYear();
  var m = oneweekdate.getMonth() + 1;
  var d = oneweekdate.getDate();
  return y + '-' + m + '-' + d
}
// 获取上月
export const preMonth = () => {
  var nowdate = new Date();
  nowdate.setMonth(nowdate.getMonth() - 1)
  return nowdate.getFullYear() + '-0' + (nowdate.getMonth() + 1)
}
// 中国标准时间转换
export const standardTime = (d) => {
  let time = new Date(d);
  let datetime =
    time.getFullYear() +
    "-" +
    format((time.getMonth() + 1)) +
    "-" +
    format(time.getDate())
  return datetime;
}
// 获取当天事件0.0.0 组件8.00
export const getStartTime = (key) => {
  return new Date(key).getTime() - 3600000 * 8
}
// 获取当天事件23.59.59 组件8.00
export const getEndTime = (key) => {
  return new Date(key).getTime() - 3600000 * 8 + 24 * 60 * 60 * 1000 - 1
}
// echarts
export const initCharts = function (chartsDate, dom, title, chartsData, legendData, type, sensorType, subTitle, smallType) {
  let wdType = undefined
  if (title) {
    wdType = title.indexOf('温度') != -1 ? 1 : ''
  }
  var myChart = echarts.init(document.getElementById(dom))
  // 绘制图表
  let optionObj = {
    tooltip: {
      trigger: 'axis',
      backgroundColor: 'rgba(0,0,0,0.6)',
      borderRadius: 4,
      extraCssText: type == 4 ? 'width:auto;height:auto;' : 'width:200px;height:auto;',
      textStyle: {
        color: '#00ffc4',
        fontSize: '14px'
      },
      position: function (pos, params, dom, rect, size) {
        var obj = {}
        if (type == 4 || type == 5 || type == 6) {
          obj.top = -10
        } else {
          obj.top = 60
        }
        obj[['left', 'right'][+(pos[0] < size.viewSize[0] / 2)]] = 5
        return obj
      },
      formatter: function (params) {
        let date = params[0].name,
          content = new Array(),
          str = ''
        if (type == 1 || type == 5) {
          for (let index = 0; index < params.length; index++) {
            const element = params[index];
            let msg = element.value == null ? `${element.marker}` + element.seriesName + ' 当前设备离线' + '<br>' : `${element.marker}` + element.seriesName + ' ' + element.value + symbolFun(sensorType, smallType, wdType) + '<br>'
            content.push(msg)
          }
        } else if (type == 2 || type == 6) {
          let alarm = '',
            warn = '',
            mesure = '',
            alarmArr = [], // 报警数据
            warnArr = [] // 预警数据
          for (let index = 0; index < params.length; index++) {
            const element = params[index]
            if (element.seriesName == '测量') {
              if (element.value == undefined) {
                mesure = '当前时段设备离线'
              } else {
                mesure = '测量: ' + element.value + symbolFun(sensorType, smallType, wdType) + '<br>'
              }
            }
            if (mesure != '当前时段设备离线') {
              if (element.seriesName == '报警') {
                alarmArr.push(element)
                if (element.value != null && alarmArr.length == 2) {
                  alarm = '报警: ' + alarmArr[0].value + '/' + alarmArr[1].value + symbolFun(sensorType, smallType, wdType) + '<br>'
                } else {
                  alarm = ''
                }
              }
              if (element.seriesName == '预警') {
                warnArr.push(element)
                if (element.value != null && warnArr.length == 2) {
                  warn = '预警: ' + warnArr[0].value + '/' + warnArr[1].value + symbolFun(sensorType, smallType, wdType) + '<br>'
                } else {
                  warn = ''
                }
              }
            } else {
              alarm = ''
              warn = ''
            }
          }
          content.push(mesure)
          content.push(warn)
          content.push(alarm)
        } else if (type == 3) {
          for (let index = 0; index < params.length; index++) {
            const element = params[index];
            let msg = element.value == null ? element.seriesName + ' 当前设备离线' + '<br>' : element.seriesName + ': ' + element.value + symbolFun(sensorType, smallType, wdType) + '<br>'
            content.push(msg)
          }
        } else if (type == 4) {
          if (params.length < 4) {
            for (let index = 0; index < params.length; index++) {
              const element = params[index];
              let msg = element.value == null ? `${element.marker}` + element.seriesName + ' 当前设备离线' + '<br>' : `${element.marker}` + element.seriesName + ' ' + element.value + symbolFun(sensorType, smallType, wdType) + '<br>'
              content.push(msg)
            }
          } else {
            for (let index = 0; index < params.length; index++) {
              const element = params[index];
              let msg
              if (index % 2 == 0 || index == 0) {
                msg = element.value == null ? `${element.marker}` + element.seriesName + ' 当前设备离线' + '&nbsp;&nbsp;' : `${element.marker}` + element.seriesName + ' ' + element.value + symbolFun(sensorType, smallType, wdType) + '&nbsp;&nbsp;'
              } else {
                msg = element.value == null ? `${element.marker}` + element.seriesName + ' 当前设备离线' + '<br>' : `${element.marker}` + element.seriesName + ' ' + element.value + symbolFun(sensorType, smallType, wdType) + '<br>'
              }
              content.push(msg)
            }
          }
        }
        for (let index = 0; index < content.length; index++) {
          const element = content[index];
          str += element
        }
        return date + '<br>' + str
      },
    },
    // 图表标题
    title: {
      text: title,
      subtext: subTitle,
      align: 'left',
      x: 8,
      textStyle: {
        fontFamily: 'PingFangSC',
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16
      }
    },
    // 图例标题
    legend: {
      data: legendData,
      right: '5%',
      itemWidth: 8, // 图例图形宽度
      itemHeight: 0, // 图例图形高度
      show: type == 2 ? true : false,
      textStyle: {
        color: '#fff'
      }
    },
    // X轴
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: chartsDate,
      axisLabel: {
        fontSize: 8,
        color: '#fff',
        formatter: function (params) {
          if (type == 4 || type == 5 || type == 6) {
            if (params.split('  ')[1]) {
              return params.split('  ')[0] + '\n' + params.split('  ')[1]
            } else {
              return params
            }
          } else {
            return params
          }
        }
      }
    },
    // y轴
    yAxis: {
      type: 'value',
      boundaryGap: ['10%', '10%'],
      splitLine: {
        show: true, // 默认显示，属性show控制显示与否
        lineStyle: {
          width: 0.5,
          type: 'dashed'
        }
      },
      axisLabel: {
        fontSize: 10,
        color: '#fff'
      }
    },
    // 图表缩放控件
    // dataZoom: [{
    //   show: type == 4 || type == 5 || type == 6 ? false : true,
    //   filterMode: 'weakFilter',
    //   backgroundColor: 'rgba(47,69,84,0)',
    //   type: 'slider',
    //   borderColor: '#ddd',
    //   right: '5%',
    //   left: '5%',
    //   height: '15',
    //   textStyle: {
    //     color: 'rgba(0, 0, 0, 0.2)'
    //   }
    // }],
    dataZoom: {
      type: 'inside'
    },
    series: chartsData
  }
  if (type == 4 || type == 5 || type == 6) {
    optionObj.grid = {
      left: '5%',
      right: '5%',
      bottom: '20%',
      top: '3%',
      containLabel: false
    }
  }
  myChart.setOption(optionObj)
}
// 曲线颜色
export const colorFlag = function (index) {
  let color = ''
  switch (index) {
    case 0:
      color = '#5B8FF9'
      break;
    case 1:
      color = '#5AD8A6'
      break;
    case 2:
      color = '#FF99C3'
      break;
    case 3:
      color = '#F6BD16'
      break;
    case 4:
      color = '#E86452'
      break;
    case 5:
      color = '#6DC8EC'
      break;
    case 6:
      color = '#945FB9'
      break;
    case 7:
      color = '#FF9845'
      break;
    case 8:
      color = '#1E9493'
      break;
    case 9:
      color = '#5D7092'
      break;
  }
  return color
}
// 判断是当月第几周
export const getMonthWeek = function (str) {
  str = Date.parse(str);
  let date = new Date(str);
  let month = date.getMonth() + 1;
  let year = date.getFullYear();
  let dateN = new Date(str);
  //月份第一天
  dateN = new Date(dateN.setDate(1));
  let w1 = dateN.getDay();
  // 将字符串转为标准时间格式
  let w = date.getDay(); //周几
  if (w === 0 && w1 != 0) { //当月第一天不是周天，当前日期是周天
    w = 7;
  }
  let week = Math.ceil((date.getDate() + 6 - w) / 7);
  if (w1 != 1) //当月第一天不是周一
    week = Math.ceil((date.getDate() + 6 - w) / 7) - 1;
  var cNum = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
  if (week === 0) { //第0周归于上月的最后一周
    month = date.getMonth();
    if (month === 0) { //跨年
      month = 12;
      year = year - 1;
    }
    let dateLast = new Date(date);
    let dayLast = new Date(year, month, 0).getDate();
    let timestamp = new Date(year, month - 1, dayLast);
    w = new Date(timestamp).getDay(); //周几
    if (w === 0) {
      w = 7;
    }
    week = Math.ceil((timestamp.getDate() + 6 - w) / 7) - 1;
  }
  week = cNum[week];
  let time = year + "年" + month + "月第" + week + "周";
  return time
};
// 更改时间弹框class 防止影响其他样式
export const changePickerClass = function () {
  setTimeout(() => {
    document
      .querySelectorAll('.ant-calendar-picker-container')
      .forEach((element) => {
        element.parentNode.setAttribute('class', 'foundationPitPicker')
      })
  }, 10);
}
// 单点位重组数据
export const chartsDataRegroup = function (data, type) {
  let sensorType = data[0].sensorType
  let obj = {
    result: {},
    chartsDate: [] // 时间
  } // 返回数据
  let YLOnceData = [], // 雨量时
    YLTotalData = [], // 雨量天
    onceData = [], // 单次数据
    oncePositiveAlarmData = [], // 单次警告正数据
    onceNegativeAlarmData = [], // 单次警告负数据
    oncePositiveWarnData = [], // 单次预警正数据
    onceNegativeWarnData = [], // 单次预警负数据
    totalData = [], // 累计数据
    totalPositiveAlarmData = [], // 累计警告正数据
    totalNegativeAlarmData = [], // 累计警告负数据
    totalPositiveWarnData = [], // 累计预警正数据
    totalNegativeWarnData = [], // 累计预警负数据
    XonceData = [], // X单次
    XoncePositiveAlarmData = [], // X单次警告正数据
    XonceNegativeAlarmData = [], // X单次警告负数据
    XoncePositiveWarnData = [], // X单次预警正数据
    XonceNegativeWarnData = [], // X单次预警负数据
    XtotalData = [], // X累计
    XtotalPositiveAlarmData = [], // X累计警告正数据
    XtotalNegativeAlarmData = [], // X累计警告负数据
    XtotalPositiveWarnData = [], // X累计预警正数据
    XtotalNegativeWarnData = [], // X累计预警负数据
    YonceData = [], // Y单次
    YoncePositiveAlarmData = [], // Y单次警告正数据
    YonceNegativeAlarmData = [], // Y单次警告负数据
    YoncePositiveWarnData = [], // Y单次预警正数据
    YonceNegativeWarnData = [], // Y单次预警负数据
    YtotalData = [], // Y累计
    YtotalPositiveAlarmData = [], // Y累计警告正数据
    YtotalNegativeAlarmData = [], // Y累计警告负数据
    YtotalPositiveWarnData = [], // Y累计预警正数据
    YtotalNegativeWarnData = [] // Y累计预警负数据
  for (let index = 0; index < data.length; index++) {
    const element = data[index]
    YLOnceData.push(element.horizontalMesureValue) // 雨量时
    YLTotalData.push(element.verticalMesureValue) // 雨量天
    obj.chartsDate.push(dateFilter(element.mesureEndTime / 1000, 6)) // 时间
    // 单次
    onceData.push(element.singleTimeMesureValue) // 单次数据
    // if (!isNull(element.singleTimeAlarmValue)) {
    //   oncePositiveAlarmData.push(element.singleTimeAlarmValue) // 单次报警正数据
    //   onceNegativeAlarmData.push(-element.singleTimeAlarmValue) // 单次报警负数据
    // } else {
    //   oncePositiveAlarmData.push(null) // 单次报警正数据
    //   onceNegativeAlarmData.push(null) // 单次报警负数据
    // }
    oncePositiveAlarmData.push(element.singleTimeUpAlarmValue) // 单次报警上限
    onceNegativeAlarmData.push(element.singleTimeDownAlarmValue) // 单次报警下限
    // if (!isNull(element.singleTimeWarnValue)) {
    //   oncePositiveWarnData.push(element.singleTimeWarnValue) // 单次预警正数据
    //   onceNegativeWarnData.push(-element.singleTimeWarnValue) // 单次预警负数据
    // } else {
    //   oncePositiveWarnData.push(null) // 单次预警正数据
    //   onceNegativeWarnData.push(null) // 单次预警负数据
    // }
    oncePositiveWarnData.push(element.singleTimeUpWarnValue) // 单次预警上限
    onceNegativeWarnData.push(element.singleTimeDownWarnValue) // 单次预警下限
    // 累计
    totalData.push(element.cumulativeMesureValue) // 累计数据
    // if (!isNull(element.cumulativeMesureAlarmValue)) {
    //   totalPositiveAlarmData.push(element.cumulativeMesureAlarmValue) // 累计报警正数据
    //   totalNegativeAlarmData.push(-element.cumulativeMesureAlarmValue) // 累计报警负数据
    // } else {
    //   totalPositiveAlarmData.push(null) // 累计报警正数据
    //   totalNegativeAlarmData.push(null) // 累计报警负数据
    // }
    totalPositiveAlarmData.push(element.cumulativeMesureUpAlarmValue) // 累计报警上限
    totalNegativeAlarmData.push(element.cumulativeMesureDownAlarmValue) // 累计报警下限
    // if (!isNull(element.cumulativeMesureWarnValue)) {
    //   totalPositiveWarnData.push(element.cumulativeMesureWarnValue) // 累计预警正数据
    //   totalNegativeWarnData.push(-element.cumulativeMesureWarnValue) // 累计预警负数据
    // } else {
    //   totalPositiveWarnData.push(null) // 累计预警正数据
    //   totalNegativeWarnData.push(null) // 累计预警负数据
    // }
    totalPositiveWarnData.push(element.cumulativeMesureUpWarnValue) // 累计预警上限
    totalNegativeWarnData.push(element.cumulativeMesureDownWarnValue) // 累计预警下限
    // X单次
    XonceData.push(element.singleTimeHorizontalMesureValue) // X单次数据
    // if (!isNull(element.singleTimeHorizontalAlarmValue)) {
    //   XoncePositiveAlarmData.push(element.singleTimeHorizontalAlarmValue) // X单次警告正数据
    //   XonceNegativeAlarmData.push(-element.singleTimeHorizontalAlarmValue) // X单次警告负数据
    // } else {
    //   XoncePositiveAlarmData.push(null) // X单次警告正数据
    //   XonceNegativeAlarmData.push(null) // X单次警告负数据
    // }
    XoncePositiveAlarmData.push(element.singleTimeHorizontalUpAlarmValue) // X单次警告上限
    XonceNegativeAlarmData.push(element.singleTimeHorizontalDownAlarmValue) // X单次警告下限
    // if (!isNull(element.singleTimeHorizontalWarnValue)) {
    //   XoncePositiveWarnData.push(element.singleTimeHorizontalWarnValue) // X单次预警正数据
    //   XonceNegativeWarnData.push(-element.singleTimeHorizontalWarnValue) // X单次预警负数据
    // } else {
    //   XoncePositiveWarnData.push(null) // X单次预警正数据
    //   XonceNegativeWarnData.push(null) // X单次预警负数据
    // }
    XoncePositiveWarnData.push(element.singleTimeHorizontalUpWarnValue) // X单次预警上限
    XonceNegativeWarnData.push(element.singleTimeHorizontalDownWarnValue) // X单次预警下限
    // X累计
    XtotalData.push(element.cumulativeHorizontalMesureValue) // X累计
    // XtotalPositiveAlarmData.push(
    //   element.cumulativeMesureHorizontalAlarmValue
    // ) // X累计警告正数据
    // XtotalNegativeAlarmData.push(
    //   -element.cumulativeMesureHorizontalAlarmValue
    // ) // X累计警告负数据
    XtotalPositiveAlarmData.push(element.cumulativeMesureHorizontalUpAlarmValue) // X累计警告上限
    XtotalNegativeAlarmData.push(element.cumulativeMesureHorizontalDownAlarmValue) // X累计警告下限
    // XtotalPositiveWarnData.push(element.cumulativeMesureHorizontalWarnValue) // X累计预警正数据
    // XtotalNegativeWarnData.push(
    //   -element.cumulativeMesureHorizontalWarnValue
    // ) // X累计预警负数据
    XtotalPositiveWarnData.push(element.cumulativeMesureHorizontalUpWarnValue) // X累计预警上限
    XtotalNegativeWarnData.push(element.cumulativeMesureHorizontalDownWarnValue) // X累计预警下限
    // Y单次
    YonceData.push(element.singleTimeVerticalMesureValue) // Y单次
    // YoncePositiveAlarmData.push(element.singleTimeVerticalAlarmValue) // Y单次警告正数据
    // YonceNegativeAlarmData.push(-element.singleTimeVerticalAlarmValue) // Y单次警告负数据
    YoncePositiveAlarmData.push(element.singleTimeVerticalUpAlarmValue) // Y单次警告上限
    YonceNegativeAlarmData.push(element.singleTimeVerticalDownAlarmValue) // Y单次警告下限
    // YoncePositiveWarnData.push(element.singleTimeVerticalWarnValue) // Y单次预警正数据
    // YonceNegativeWarnData.push(-element.singleTimeVerticalWarnValue) // Y单次预警负数据
    YoncePositiveWarnData.push(element.singleTimeVerticalUpWarnValue) // Y单次预警上限
    YonceNegativeWarnData.push(element.singleTimeVerticalDownWarnValue) // Y单次预警下限
    // Y累计
    YtotalData.push(element.cumulativeVerticalMesureValue) // Y累计
    // YtotalPositiveAlarmData.push(element.cumulativeMesureVerticalAlarmValue) // Y累计警告正数据
    // YtotalNegativeAlarmData.push(
    //   -element.cumulativeMesureVerticalAlarmValue
    // ) // Y累计警告负数据
    YtotalPositiveAlarmData.push(element.cumulativeMesureVerticalUpAlarmValue) // Y累计警告上限
    YtotalNegativeAlarmData.push(
      element.cumulativeMesureVerticalDownAlarmValue
    ) // Y累计警告下限
    // YtotalPositiveWarnData.push(element.cumulativeMesureVerticalWarnValue) // Y累计预警正数据
    // YtotalNegativeWarnData.push(-element.cumulativeMesureVerticalWarnValue) // Y累计预警负数据
    YtotalPositiveWarnData.push(element.cumulativeMesureVerticalUpWarnValue) // Y累计预警上限
    YtotalNegativeWarnData.push(element.cumulativeMesureVerticalDownWarnValue) // Y累计预警下限
  }
  switch (type) {
    case 1:
      // type 1->单次
      if (sensorType == 'Yuliang') {
        // 雨量单独判断 单点位曲线，数据源为多点位数据源
        obj.result = {
          data: YLOnceData,
          alarmPositiveData: XtotalPositiveAlarmData,
          alarmNegativeData: XtotalNegativeAlarmData,
          warnPositiveData: XtotalPositiveWarnData,
          warnNegativeData: XtotalNegativeWarnData
        }

      } else {
        obj.result = {
          data: onceData,
          alarmPositiveData: oncePositiveAlarmData,
          alarmNegativeData: onceNegativeAlarmData,
          warnPositiveData: oncePositiveWarnData,
          warnNegativeData: onceNegativeWarnData
        }
      }
      break;
    case 2:
      // 2->累计
      if (sensorType == 'Yuliang') {
        // 雨量单独判断 单点位曲线，数据源为多点位数据源
        obj.result = {
          data: YLTotalData,
          alarmPositiveData: YtotalPositiveAlarmData,
          alarmNegativeData: YtotalNegativeAlarmData,
          warnPositiveData: YtotalPositiveWarnData,
          warnNegativeData: YtotalNegativeWarnData
        }
      } else {
        obj.result = {
          data: totalData,
          alarmPositiveData: totalPositiveAlarmData,
          alarmNegativeData: totalNegativeAlarmData,
          warnPositiveData: totalPositiveWarnData,
          warnNegativeData: totalNegativeWarnData
        }
      }
      break;
    case 3:
      // 3->X单次
      obj.result = {
        data: XonceData,
        alarmPositiveData: XoncePositiveAlarmData,
        alarmNegativeData: XonceNegativeAlarmData,
        warnPositiveData: XoncePositiveWarnData,
        warnNegativeData: XonceNegativeWarnData
      }
      break;
    case 4:
      // 4->X累计
      obj.result = {
        data: XtotalData,
        alarmPositiveData: XtotalPositiveAlarmData,
        alarmNegativeData: XtotalNegativeAlarmData,
        warnPositiveData: XtotalPositiveWarnData,
        warnNegativeData: XtotalNegativeWarnData
      }
      break;
    case 5:
      // 5->Y单次
      obj.result = {
        data: YonceData,
        alarmPositiveData: YoncePositiveAlarmData,
        alarmNegativeData: YonceNegativeAlarmData,
        warnPositiveData: YoncePositiveWarnData,
        warnNegativeData: YonceNegativeWarnData
      }
      break;
    case 6:
      // 6->Y累计
      obj.result = {
        data: YtotalData,
        alarmPositiveData: YtotalPositiveAlarmData,
        alarmNegativeData: YtotalNegativeAlarmData,
        warnPositiveData: YtotalPositiveWarnData,
        warnNegativeData: YtotalNegativeWarnData
      }
      break;
  }
  return obj
}
// 符号
export const symbolFun = function (type, smallType, wdType) {
  if (type && smallType) {
    switch (type) {
      case 'WaterData':
        return 'm'
        break
      case 'CollWeiyiPoint':
      case 'Yuliang':
        return 'mm'
        break
      case 'ZhenXianTempData':
      case 'CollZhouliPoint':
      case 'YingLi':
        if (smallType == 85 || smallType == 86) {
          return 'Mpa'
        } else {
          return 'KN'
        }
        break
      case 'CollQingxiePoint':
        if (smallType == 35 || smallType == 'small_type_004') {
          return 'mm'
        } else {
          return '°'
        }
        break
      case 'Trwsd':
        if (wdType == 1) {
          return '°C'
        } else {
          return '%'
        }
        break
    }
  } else {
    return ''
  }
}
// 字体颜色
export const colorFun = function (value, upWarnValue, downWarnValue, upAlarmValue, downAlarmValue) {
  if (isNull(upWarnValue) && isNull(downWarnValue) && isNull(upAlarmValue) && isNull(downAlarmValue)) {
    // 无预警报警值
    return ['获取', '#427bf4']
  } else {
    if (
      Number(value) > Number(upAlarmValue) ||
      Number(value) == Number(upAlarmValue) ||
      Number(value) < Number(downAlarmValue) ||
      Number(value) == Number(downAlarmValue)
    ) {
      // 大于报警上限 等于报警上限 小于报警下限 等于报警下限
      return ['报警', '#fa2b4e']
    } else if (
      (Number(value) < Number(upAlarmValue) ||
        Number(value) > Number(downAlarmValue)) &&
      (Number(value) > Number(upWarnValue) ||
        Number(value) == Number(upWarnValue) ||
        Number(value) < Number(downWarnValue) ||
        Number(value) == Number(downWarnValue))
    ) {
      // 报警区间 大于预警上限 等于预警上限 小于预警下限 等于预警下限
      return ['预警', '#fa9a2f']
    } else if (Number(value) < Number(upWarnValue) ||
      Number(value) > Number(downWarnValue)) {
      // 小于预警上限 大于预警下限
      return ['获取', '#427bf4']
    }
  }
}
// 获取charts title
export const getChartsTitle = function (sensorType) {
  let titleArr = []
  switch (sensorType) {
    case 'WaterData':
      titleArr = ['水位']
      break;
    case 'ZhenXianTempData':
      titleArr = ['锚杆拉力']
      break;
    case 'CollWeiyiPoint':
      titleArr = ['位移']
      break;
    case 'CollZhouliPoint':
      titleArr = ['轴力']
      break;
    case 'Yuliang':
      titleArr = ['时', '天']
      break;
    case 'CollQingxiePoint':
      titleArr = ['X方向', 'Y方向']
      break;
    case 'Trwsd':
      titleArr = ['温度', '湿度']
      break;
  }
  return titleArr
}
export const getYearWeek = (a, b, c) => {
  //date1是当前日期
  //date2是当年第一天
  //d是当前日期是今年第多少天
  //用d + 当前年的第一天的周差距的和在除以7就是本年第几周
  var date1 = new Date(a, parseInt(b) - 1, c),
    date2 = new Date(a, 0, 1),
    d = Math.round((date1.valueOf() - date2.valueOf()) / 86400000);
  return Math.ceil((d + ((date2.getDay() + 1) - 1)) / 7);
};
export const getDate = function (year) {
  var dates = []
  for (var i = 1; i <= 12; i++) {
    for (var j = 1; j <= new Date(year, i, 0).getDate(); j++) {
      dates.push(year + '-' + formatNumber(i) + '-' + formatNumber(j) + '_' + new Date([year, i, j].join('-')).getDay())
    }
  }
  return dates
}