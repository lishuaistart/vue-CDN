/*
 * @Description:帆软表单端口配置
 * @Version: 1.0.0
 * @Author: 杜凡
 * @Date: 2021-02-01 19:19:19
 * @LastEditors: 赵亮
 * @LastEditTime: 2021-06-24 19:03:02
 */
//默认开发环境 30114
let FR = process.env.VUE_APP_VERSION === 'standard' ? 'spm3.0/deep/dev' : '8J_south/deep/dev'
//测试环境 31114
if (process.env.VUE_APP_NODE_ENV === 'test') {
  FR = '8J_south/deep/test'
  if (process.env.VUE_APP_VERSION === 'standard') {
    // 3.0标版
    FR = 'spm3.0/deep/test'
  }
}
//灰度环境 31014
else if (process.env.VUE_APP_NODE_ENV === 'presentation') {
  FR = '8J_south/deep/pre'
  if (process.env.VUE_APP_VERSION === 'standard') {
    // 3.0标版
    FR = 'spm3.0/deep/pre'
  }
}
//生产环境 端口未定
else if (process.env.VUE_APP_NODE_ENV === 'production') {
  FR = '8J_south/deep/pro'
  if (process.env.VUE_APP_VERSION === 'standard') {
    // 3.0标版
    FR = 'spm3.0/deep/pro'
  }
}
export default FR;