/*
 * @Description:
 * @Version: 1.0.0
 * @Author: 李晨光
 * @Date: 2020-10-10 16:15:31
 * @LastEditors: 李帅
 * @LastEditTime: 2021-07-17 13:44:52
 */
import { publicRoute, protectedRoute } from './config'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
const routes = publicRoute.concat(protectedRoute)
import Cookie from 'js-cookie'
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err)
}
const router = new VueRouter({
  mode: 'hash',
  linkActiveClass: 'active',
  routes: routes,
})
// router gards
router.beforeEach((to, from, next) => {
  //auth route is authenticated
  const monitorJwt = Cookie.get('monitorJwt')
  // 重置组件缓存
  let toDepth = to.path.split('/').length
  let fromDepth = from.path.split('/').length
  if (toDepth <= fromDepth) {
    from.meta.keepAlive = false
    to.meta.keepAlive = true
  }

  NProgress.start()
  next()
})

router.afterEach((to, from) => {
  NProgress.done()
})

export default router
