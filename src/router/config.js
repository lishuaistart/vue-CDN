/*
 * @Description: 路由配置
 * @Version: 1.0.0
 * @Author: 李晨光
 * @Date: 2021-03-22 10:29:18
 * @LastEditors: 李帅
 * @LastEditTime: 2021-07-17 14:21:56
 */

export const publicRoute = [
  {
    path: '*',
    component: () =>
      import(/* webpackChunkName: "errors-404" */ 'views/error/404.vue'),
  },
  {
    path: '/404',
    name: '404',
    meta: {
      title: 'Not Found',
    },
    component: () =>
      import(/* webpackChunkName: "errors-404" */ 'views/error/404.vue'),
  },
  {
    path: '/500',
    name: '500',
    meta: {
      title: 'Server Error',
    },
    component: () =>
      import(/* webpackChunkName: "errors-500" */ 'views/error/500.vue'),
  },
  {
    path: '/403',
    name: '403',
    meta: {
      title: 'Server Error',
    },
    component: () =>
      import(/* webpackChunkName: "errors-403" */ 'views/error/403.vue'),
  },
]

export const protectedRoute = [
  {
    path: '/',
    name: 'Home',
    component: () => import('views/Home'),
    meta: { name: '首页' },
  },
]
