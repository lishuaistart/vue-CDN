/*
 * @Description:
 * @Version: 1.0.0
 * @Author: 李帅
 * @Date: 2021-07-17 11:01:07
 * @LastEditors: 李帅
 * @LastEditTime: 2021-07-17 14:20:18
 */
import App from './App.vue'
import router from './router'
import store from './vuex'
import './core/lazy_use'
import 'assets/css/reset.css'
import 'assets/css/common.css'
import 'assets/css/antd-reset.css'
import 'assets/css/animation.css'
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
