/*
 * @Descripttion: 
 * @Version: 1.0.0
 * @Author: 李晨光
 * @Date: 2020-07-04 11:50:39
 * @LastEditors: 李晨光
 * @LastEditTime: 2020-07-07 11:17:33
 */
import Vue from 'vue'
import VueStorage from 'vue-ls'
import config from '#/config/defaultSettings'

// base library
import Antd from 'ant-design-vue'
import VueCropper from 'vue-cropper'
import 'ant-design-vue/dist/antd.less'



// ext library
import VueClipboard from 'vue-clipboard2'
// import '@/components/use'
import './directives/action'

VueClipboard.config.autoSetContainer = true

Vue.use(Antd)
Vue.use(VueStorage, config.storageOptions)
Vue.use(VueClipboard)
Vue.use(VueCropper)

process.env.NODE_ENV !== 'production' && console.warn('[antd-pro] WARNING: Antd now use fulled imported.')