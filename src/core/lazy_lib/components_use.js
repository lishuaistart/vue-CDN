/*
 * @Descripttion:
 * @Version: 1.0.0
 * @Author: 李晨光
 * @Date: 2020-07-06 14:04:04
 * @LastEditors: 任笠
 * @LastEditTime: 2021-03-26 14:55:28
 */
/* eslint-disable */
/**
 * 该文件是为了按需加载，剔除掉了一些不需要的框架组件。
 * 减少了编译支持库包大小
 *
 * 当需要更多组件依赖时，在该文件加入即可
 */
import {
  ConfigProvider,
  Layout,
  Breadcrumb,
  Input,
  InputNumber,
  Button,
  Switch,
  Radio,
  Checkbox,
  Select,
  Modal,
  Table,
  Tabs,
  Icon,
  Popover,
  Dropdown,
  List,
  Avatar,
  Spin,
  Menu,
  Tooltip,
  Tag,
  Divider,
  DatePicker,
  TimePicker,
  Progress,
  message,
  Pagination,
  Tree,
  AutoComplete,
  Cascader,
  Carousel,
  Timeline,
  Steps,
  Drawer,
  Row,
  Col,
  Card,
  Skeleton,
  Popconfirm,
  Form,
  FormModel,
  Upload,
  Affix,
  Space,
  TreeSelect
} from 'ant-design-vue'

import { scrollBoard } from '@jiaminghi/data-view'


Vue.use(scrollBoard)
// import VueCropper from 'vue-cropper'
Vue.use(ConfigProvider)
Vue.use(Layout)
Vue.use(Breadcrumb)
Vue.use(Input)
Vue.use(InputNumber)
Vue.use(Button)
Vue.use(Switch)
Vue.use(Radio)
Vue.use(Checkbox)
Vue.use(Select)
Vue.use(Modal)
Vue.use(Table)
Vue.use(Tabs)
Vue.use(Icon)
Vue.use(Popover)
Vue.use(Dropdown)
Vue.use(List)
Vue.use(Avatar)
Vue.use(Spin)
Vue.use(Menu)
Vue.use(Tooltip)
Vue.use(Tag)
Vue.use(Divider)
Vue.use(DatePicker)
Vue.use(TimePicker)
Vue.use(Progress)
Vue.use(Pagination)
Vue.use(Tree)
Vue.use(AutoComplete)
Vue.use(Cascader)
Vue.use(Carousel)
Vue.use(Timeline)
Vue.use(Steps)
Vue.use(Drawer)
Vue.use(Row)
Vue.use(Col)
Vue.use(Card)
Vue.use(Skeleton)
Vue.use(Popconfirm)
Vue.use(Form)
Vue.use(FormModel)
Vue.use(Upload)
Vue.use(Affix)
Vue.use(Space)
Vue.use(TreeSelect)

Vue.prototype.$confirm = Modal.confirm
message.config({
  top: `150px`,
  duration: 2,
  maxCount: 3,
})
Vue.prototype.$message = message
Vue.prototype.$info = Modal.info
Vue.prototype.$success = Modal.success
Vue.prototype.$error = Modal.error
Vue.prototype.$warning = Modal.warning

//自定义 font 图标
const IconFont = Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_1271362_8sk6l2crmdb.js',
})
const IconFont2 = Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_1986849_uznu0qaqrdm.js',
})
const IconFont3 = Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_2058337_facjl8ubydc.js',
})

Vue.component('icon-font', {
  ...IconFont,
  ...IconFont2,
  ...IconFont3,
})

import Loading from '../lazy_lib/loading/index'
import Vue from 'vue'
Vue.use(Loading)
