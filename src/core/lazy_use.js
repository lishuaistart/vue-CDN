/*
 * @Descripttion:
 * @Version: 1.0.0
 * @Author: 李晨光
 * @Date: 2020-07-06 14:04:04
 * @LastEditors: 李晨光
 * @LastEditTime: 2021-03-31 11:54:55
 */

// base library
import '#/core/lazy_lib/components_use'
// ext library
import './directives/action'
import vueWaves from './directives/waves/waves.directive'

import vfilter from './filters/vfilter'
import Util from '#/assets/js/util'
import Common from '#/assets/js/common'
import 'assets/js/mixin/eventBus'
import VueLazyload from 'vue-lazyload'
import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'
import scroll from 'vue-seamless-scroll'
Vue.use(scroll)
// 引入图片裁剪
// import Croppa from 'vue-croppa'
// 图片懒加载
Vue.use(VueLazyload, {
  preLoad: 1.3,
  // error: require('#/assets/image/common/loaded-failed.png'),
  loading: require('#/assets/image/common/loading.gif'),
  attempt: 1,
})
// 图片预览
Vue.use(Viewer)
Viewer.setDefaults({
  zIndexInline: 9999,
  inline: false,
  button: true,
  navbar: false,
  title: false,
  toolbar: true,
  tooltip: false,
  movable: true,
  zoomable: true,
  rotatable: true,
  scalable: false,
  transition: true,
  fullscreen: false,
  keyboard: false,
})
// 剪切板
// 引入点击水波纹指令
Vue.use(vueWaves)

// 注册过滤器
for (let key in vfilter.vfilter) {
  Vue.filter(key, vfilter.vfilter[key])
}

// 公用方法
Vue.use(Util)
Vue.use(Common)

// 引入请求方式
import {
  get,
  post,
  postFD,
  uploadFile,
  getAll,
  exportFile,
  exportFileFD,
} from '#/assets/js/http'

// 在vue原型链上注册 axios
// get请求
Vue.prototype.$get = get
// post请求
Vue.prototype.$post = post
// postFD请求
Vue.prototype.$postFD = postFD
// uploadFile请求
Vue.prototype.$uploadFile = uploadFile
//导出
Vue.prototype.$exportFile = exportFile
Vue.prototype.$exportFileFD = exportFileFD

// 多并发请求
Vue.prototype.$getAll = getAll

Vue.prototype.moment = moment

process.env.NODE_ENV !== 'production' &&
  console.warn('[antd-pro] NOTICE: Antd use lazy-load.')
